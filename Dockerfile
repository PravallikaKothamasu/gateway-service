FROM openjdk:8-jre
COPY target /gatewayservice
WORKDIR /gatewayservice
ENTRYPOINT ["java", "-jar", "gatewayservice-0.0.1-SNAPSHOT.jar"]